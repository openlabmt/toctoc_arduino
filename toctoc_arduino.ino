// Toctoc Arduino
// Receives a 433 Hz signal (code) from a remote control
// and actuates a servor motor that pulls a wire linked to a door

#include <Servo.h> 
#include <RCSwitch.h>
 
Servo myservo;  // create servo object to control a servo 
                // a maximum of eight servo objects can be created 
                
const byte LED_PIN = 6;
const byte SERVO_PIN = 5;
const int RECV_CODE = 21845; // Remote control code
int pos = 0;    // variable to store the servo position 

RCSwitch mySwitch = RCSwitch();

void setup() { 
    mySwitch.enableReceive(0); // Receiver on interrupt 0 => that is pin #2
    myservo.attach(SERVO_PIN); // attaches the servo on pin 9 to the servo object 
    Serial.begin(9600);
    pinMode(LED_PIN, OUTPUT);

    // Reset the servo motor position to 0
    if (pos >= 0) {
        for(pos >= 0; pos>=0; pos-=1) {
          myservo.write(pos);
        }
    }
    else {
        for(pos < 0; pos<=0; pos+=1) {
          myservo.write(pos);
        }
    }
}
 
void loop() { 
    if (mySwitch.available()) {
        int value = mySwitch.getReceivedValue();

        if (value == RECV_CODE) {
            //Serial.println(value);
            digitalWrite(LED_PIN, HIGH);
            runMotor();
            // Waits 1 s before receiving an other command
            delay(1000);
        }
        mySwitch.resetAvailable();
    }
    else {
        digitalWrite(LED_PIN, LOW);
    }
}

void runMotor() {
    for(pos = 0; pos < 120; pos +=1) {
        myservo.write(pos);              // tell servo to go to position in variable 'pos' 
        delay(10);                       // waits 10 ms for the servo to reach the position 
    } 

    for(pos = 120; pos>=0; pos-=1) {   // goes from 180 degrees to 0 degrees                              
        myservo.write(pos);              // tell servo to go to position in variable 'pos' 
        delay(10);                       // waits 10 ms for the servo to reach the position 
    } 
}


